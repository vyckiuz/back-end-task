<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScooterTrip extends Model
{
    use HasFactory;

    const STARTED = "Started";
    const FINISHED = "Finished";

    protected $fillable = [
        'start_time',
        'end_time',
        'start_coordinates_lat',
        'start_coordinates_long',
        'status',
        'scooter_id',
        'user_id',
    ];


    public function scooter() 
    {
        return $this->belongsTo(Scooter::class);
    }


    public function user() 
    {
        return $this->belongsTo(User::class);
    }


    public function tripRoutes()
    {
        return $this->hasMany(ScooterTripRoute::class);
    }


    public function finishTrip($end_coordinates_lat, $end_coordinates_long, $time)
    {
        $this->end_coordinates_lat = $end_coordinates_lat;
        $this->end_coordinates_long = $end_coordinates_long;
        $this->end_time = $time;
        $this->status = $this::FINISHED;
        $this->save();
    }
}
