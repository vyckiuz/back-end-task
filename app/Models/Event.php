<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    const TRIP_BEGINS = "Trip begins";
    const TRIP_ENDS = "Trip ends";

    protected $fillable = [
        'name',
    ];

    public function scooters() {
    	return $this->belongsToMany(Scooter::class, 'event_scooter')->withTimestamps();;
    }
}
