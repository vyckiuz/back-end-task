<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Transformers\ScooterTransformer;

class Scooter extends Model
{
    use HasFactory;

    public $transformer = ScooterTransformer::class;

    protected $fillable = [
        'uuid',
        'status',
        'coordinates_lat',
        'coordinates_long',
        'scooter_model_id',
        'city_id',
    ];

    // protected $hidden = [
    //     'uuid',
    // ];


    public function city()
    {
        return $this->belongsTo(City::class);
    }


    public function scooterModel()
    {
        return $this->belongsTo(ScooterModel::class);
    }


    public function events() {
        return $this->belongsToMany(Event::class, 'event_scooter')->withTimestamps();
    }


    public function trips() {
        return $this->hasMany(ScooterTrip::class);
    }


    public function scooterStatus() {
        return $this->belongsTo(ScooterStatus::class);
    }


    public function updateLocation($coordinates_lat, $coordinates_long) {
        $this->coordinates_lat = $coordinates_lat;
        $this->coordinates_long = $coordinates_long;
        $this->save();
    }
}