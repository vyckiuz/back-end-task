<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScooterTripRoute extends Model
{
    use HasFactory;

    protected $fillable = [
        'coordinates_lat',
        'coordinates_long',
        'scooter_trip_id',
        'time',
    ];


    public function scooterTrip()
    {
    	return $this->belongsTo(ScooterTrip::class);
    }
}
