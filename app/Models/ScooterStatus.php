<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScooterStatus extends Model
{
    use HasFactory;

    const AVAILABLE = "Available";
    const OCCUPIED = "Occupied";

    protected $fillable = [
        'name',
    ];

    protected $table = 'scooter_statuses';
}
