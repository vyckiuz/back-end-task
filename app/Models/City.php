<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    // Ottawa (LAT/LONG ranges):
    // LAT: 45.27 -> 45.50
    // LONG: -75.88 -> -75.50
    // 
    // Montral (LAT/LONG ranges):
    // LAT: 45.36 -> 45.67
    // LONG: -73.85 -> -73.43

    use HasFactory;

    protected $table = 'cities';

    protected $fillable = [
        'name',
    ];


    public function rand_coordinates($st_num=0,$end_num=1,$mul=1000000)
    {
        return mt_rand($st_num*$mul,$end_num*$mul)/$mul;
    }


    public function getRandomLatitude()
    {
        if ($this->name == "Ottawa")
        {
            return $this->rand_coordinates(45,46);
        }
        elseif ($this->name == "Montreal") 
        {
            return $this->rand_coordinates(45,46);
        }
    }


    public function getRandomLongitude()
    {
        if ($this->name == "Ottawa")
        {
            return $this->rand_coordinates(-76,-75);
        }
        elseif ($this->name == "Montreal") 
        {
            return $this->rand_coordinates(-74,-73);
        }
    }
}