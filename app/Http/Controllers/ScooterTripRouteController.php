<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ScooterTrip;
use App\Models\User;
use App\Models\Scooter;
use App\Models\ScooterTripRoute;

class ScooterTripRouteController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_data = $request->all();

        // Find last users not finished trip
        $user_uuid = $request_data["user_uuid"];
        $scooter_uuid = $request_data["scooter_uuid"];
        $coordinates_lat = $request_data["coordinates_lat"];
        $coordinates_long = $request_data["coordinates_long"];
        $start_time = $request_data["time"];

        $user = User::where('uuid', $user_uuid)->firstOrFail();
        $scooter = Scooter::where('uuid', $scooter_uuid)->firstOrFail();

        $scooter->updateLocation($coordinates_lat, $coordinates_long);

        $last_started_trip = $user->trips()->get()->where("status", ScooterTrip::STARTED)->last();

        $scooter_trip_route = ScooterTripRoute::create([
            'time' => $start_time,
            'coordinates_lat' => $coordinates_lat,
            'coordinates_long' => $coordinates_long,
            'scooter_trip_id' => $last_started_trip->id,
        ]);


        return response()->json(['message' => "success"], 200);      
    }
}
