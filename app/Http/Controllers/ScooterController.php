<?php

namespace App\Http\Controllers;

use App\Models\Scooter;
use App\Models\User;
use App\Models\ScooterStatus;
use Illuminate\Http\Request;
use App\Jobs\ScooterService;


function get_coordinates_from_request($coordinates_data)
{
    if(array_key_exists("start_latitude", $coordinates_data) and 
       array_key_exists("end_latitude", $coordinates_data) and 
       array_key_exists("start_longitude", $coordinates_data) and 
       array_key_exists("end_longitude", $coordinates_data)) 
    {
        $start_latitude = $coordinates_data["start_latitude"];
        $end_latitude = $coordinates_data["end_latitude"];
        $start_longitude = $coordinates_data["start_longitude"];
        $end_longitude = $coordinates_data["end_longitude"];

        if ($start_latitude > $end_latitude) 
        {
            $temp = $end_latitude;
            $end_latitude = $start_latitude;
            $start_latitude = $temp;
        }

        if ($start_longitude > $end_longitude) 
        {
            $temp = $start_longitude;
            $start_longitude = $end_longitude;
            $end_longitude = $temp;
        }

        return ["start_latitude" => $start_latitude, "end_latitude" => $end_latitude, "start_longitude" => $start_longitude, "end_longitude" => $end_longitude];
    }
    else
    {
        return False;
    }
}


class ScooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $scoters_filter_array = [];      
        $req_data = $request->all();

        if (array_key_exists("filters",$req_data)) {
            $filters = $req_data["filters"];
            if (array_key_exists("status", $filters)) { 

                $status = $req_data["filters"]["status"];

                $scooterStatus = ScooterStatus::find($status);

                if($scooterStatus) {

                    $filter = ["scooter_status_id", "=", $scooterStatus->id];
                    array_push($scoters_filter_array, $filter);
                }
            }
        }

        if (array_key_exists("coordinates",$req_data)) 
        {
            $coordinates_array = get_coordinates_from_request($req_data["coordinates"]);

            if ($coordinates_array) {
                array_push($scoters_filter_array, ["coordinates_lat", ">", $coordinates_array["start_latitude"]], 
                    ["coordinates_lat", "<", $coordinates_array["end_latitude"]],
                    ["coordinates_long", ">", $coordinates_array["start_longitude"]],
                    ["coordinates_long", "<", $coordinates_array["end_longitude"]]
                );
            }
        }

        $scooters = Scooter::where($scoters_filter_array)->get();
        
        return response()->json($scooters, 200);   

    }
}
