<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ScooterTrip;

class ScooterTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scooterTrips = ScooterTrip::with("scooter")->get();
        return response()->json($scooterTrips, 200);   
    }
}
