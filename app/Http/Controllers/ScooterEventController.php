<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ScooterTrip;
use App\Models\ScooterTripRoute;
use App\Models\User;
use App\Models\Scooter;
use App\Models\ScooterStatus;

class ScooterEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Scooter sends Trip Begins or Trip ends event
        $request_data = $request->all();
        $event_id = $request_data["event_id"];
        

        if($event_id == 1) //Trip begins event
        {
            // Attaching event to scooter_events table
            // Creating ScooterTrip Object
            // Creating SctooterTripRoute object with start point

            $start_lat = $request_data["coordinates_lat"];
            $start_long = $request_data["coordinates_long"];
            $user_uuid = $request_data["user_uuid"];
            $scooter_uuid = $request_data["scooter_uuid"];
            $start_time = $request_data["start_time"];

            $user = User::where('uuid', $user_uuid)->firstOrFail(); 

            $scooter = Scooter::where('uuid', $scooter_uuid)->firstOrFail();
            $scooter->events()->attach($event_id);
            
            $scooter->scooter_status_id = 2;
            $scooter->save();


            // Scooter start trip
            $scooter_trip = ScooterTrip::create([
                'start_time' => $start_time,
                'start_coordinates_lat' => $start_lat,
                'start_coordinates_long' => $start_long,
                'status' => ScooterTrip::STARTED,
                'scooter_id' => $scooter->id,
                'user_id' => $user->id,
            ]);

            $scooter_trip_route = ScooterTripRoute::create([
                'time' => $start_time,
                'coordinates_lat' => $start_lat,
                'coordinates_long' => $start_long,
                'scooter_trip_id' => $scooter_trip->id,
            ]);
        
            return ["message" => "Success"];
        }

        else if($event_id == 2) //Trip Ends
        {
            // Attaching event to scooter_events table
            // Finishing ScooterTrip Object
            // Creating SctooterTripRoute object with end point

            $end_lat = $request_data["coordinates_lat"];
            $end_long = $request_data["coordinates_long"];
            $user_uuid = $request_data["user_uuid"];
            $scooter_uuid = $request_data["scooter_uuid"];
            $time = $request_data["time"];

            $user = User::where('uuid', $user_uuid)->firstOrFail();

            $scooter = Scooter::where('uuid', $scooter_uuid)->firstOrFail();
            $scooter->events()->attach($event_id);
            
            $scooter->scooter_status_id = 1;
            $scooter->updateLocation($end_lat, $end_long);
            // $scooter->save();

            $last_started_trip = $user->trips()->get()->where("status", ScooterTrip::STARTED)->last();
            $last_started_trip->finishTrip($end_lat, $end_long, $time);


            $scooter_trip_route = ScooterTripRoute::create([
                'time' => $time,
                'coordinates_lat' => $end_lat,
                'coordinates_long' => $end_long,
                'scooter_trip_id' => $last_started_trip->id,
            ]);
        
            return response()->json(['message' => "success"], 200);   
        }
        else return response()->json(['message'=> "Wrong Event ID parameter value"], 400);
    }
}
