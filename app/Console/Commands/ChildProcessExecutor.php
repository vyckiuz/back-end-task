<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class ChildProcessExecutor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'executechildprocess';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $process = new Process(['php', 'artisan', 'childprocess']);
        $process2 = new Process(['php', 'artisan', 'childprocess']);
        $process3 = new Process(['php', 'artisan', 'childprocess']);

        $process->start();
        $process2->start();
        $process3->start();


        while ($process->isRunning() || $process2->isRunning() || $process2->isRunning()){
          sleep(1);
        }

        // Finally we return.
        // print($process->getOutput());
    }
}
