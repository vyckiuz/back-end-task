<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\Event;
use App\Models\ScooterTrip;
use App\Jobs\ScooterService;


class ChildProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'childprocess';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->hostname_url = env('API_URL', 'http://127.0.0.1:8000/api/');//"";
    }


    public function getScootersDataFromAPI()
    {;
        $endpoint_url = "scooters";

        $full_url = $this->hostname_url . $endpoint_url;

        $parameters = [
            "filters" => [
                'status' => 1,
            ],
        ];

        $response = Http::withToken("scooterrandomtoken")->get($full_url, $parameters);
        $scooters_data = $response->json();



        return $scooters_data;
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users_witch_active_trips = ScooterTrip::where("status", ScooterTrip::STARTED)->get()->pluck("user_id")->toArray();
        $user = User::all()->except($users_witch_active_trips)->random();
       
        //GET random user
        $user = User::all()->random();

        // 1. USER GETS Scooters list
        $scooters_data = $this->getScootersDataFromAPI();

        // GET RANDOM Scooter from list and start trip
        $random_scooter = $scooters_data[array_rand($scooters_data)];
        $endpoint_url = "scooters/events";
        $full_url = $this->hostname_url . $endpoint_url;

        $start_lat = $random_scooter["coordinates_lat"];
        $start_long = $random_scooter["coordinates_long"];

        $parameters = [
            "event_id" => 1,
            "coordinates_lat" => $start_lat,
            "coordinates_long" => $start_long,
            "user_uuid" => $user->uuid,
            "scooter_uuid" => $random_scooter["uuid"],
            "start_time" => date('Y-m-d H:i:s', time()),
        ];

        $response = Http::withToken("scooterrandomtoken")->post($full_url, $parameters);

        // Scooter moving
        // $counter = rand(10, 15) / 3
        for($index=0; $index<5; $index++) {
            $endpoint_url = "scooters/trips/routes";
            $full_url = $this->hostname_url . $endpoint_url;

            $start_lat += 0.1;
            $start_long += 0.1;

            $parameters = [
                "coordinates_lat" => $start_lat,
                "coordinates_long" => $start_long,
                "user_uuid" => $user->uuid,
                "scooter_uuid" => $random_scooter["uuid"],
                "time" => date('Y-m-d H:i:s', time()),
            ];

            $response = Http::withToken("scooterrandomtoken")->post($full_url, $parameters);
            sleep(3);
        }

        
        $start_lat += 0.1;
        $start_long += 0.1;

        // Finishing scooter trip
        $endpoint_url = "scooters/events";
        $full_url = $this->hostname_url . $endpoint_url;

        $parameters = [
            "event_id" => 2,
            "coordinates_lat" => $start_lat,
            "coordinates_long" => $start_long,
            "user_uuid" => $user->uuid,
            "scooter_uuid" => $random_scooter["uuid"],
            "time" => date('Y-m-d H:i:s', time()),
        ];

        $response = Http::withToken("scooterrandomtoken")->post($full_url, $parameters);

    }
}
