<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Scooter;

class ScooterTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Scooter $scooter)
    {
        return [
            'id' => $scooter->id,
            'city' => $scooter->city,
            'model' => $scooter->scooterModel,
            'status' => $scooter->status,
            'coordinates_lat' => $scooter->coordinates_lat,
            'coordinates_long' => $scooter->coordinates_long,
        ];
    }
}
