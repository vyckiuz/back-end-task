<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\ScooterModel;
use App\Models\City;
use App\Models\Scooter;
use App\Models\Event;
use App\Models\ScooterStatus;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(20)->create();
        ScooterModel::firstOrCreate(['name' => 'Beaster']);
        ScooterModel::firstOrCreate(['name' => 'Xiaomi']);
        City::firstOrCreate(['name' => 'Ottawa']);
        City::firstOrCreate(['name' => 'Montreal']);
        Event::firstOrCreate(['name' => Event::TRIP_BEGINS]);
        Event::firstOrCreate(['name' => Event::TRIP_ENDS]);
        ScooterStatus::firstOrCreate(['name' => ScooterStatus::AVAILABLE]);
        ScooterStatus::firstOrCreate(['name' => ScooterStatus::OCCUPIED]);
        Scooter::factory(100)->create();
    }
}
