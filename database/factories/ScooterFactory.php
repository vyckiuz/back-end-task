<?php

namespace Database\Factories;

use App\Models\Scooter;
use App\Models\ScooterModel;
use App\Models\City;
use App\Models\ScooterStatus;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ScooterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Scooter::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $city = City::all()->random();
        return [
            'uuid' => Str::random(30),
            'scooter_model_id' => ScooterModel::all()->random()->id,
            'scooter_status_id' => 1,//ScooterStatus::all()->random()->id,
            'city_id' => $city->id,
            'coordinates_lat' => $city->getRandomLatitude(),
            'coordinates_long' => $city->getRandomLongitude(),  
        ];
    }
}





