<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScooterTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scooter_trips', function (Blueprint $table) {
            $table->id();
            $table->dateTime("start_time");
            $table->dateTime("end_time")->nullable();
            $table->float("start_coordinates_lat");
            $table->float("start_coordinates_long");
            $table->float("end_coordinates_lat")->nullable();
            $table->float("end_coordinates_long")->nullable();
            $table->string("status");
            $table->foreignId('user_id')->constrained();
            $table->foreignId('scooter_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scooter_trips');
    }
}
