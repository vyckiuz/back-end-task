<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScooterTripRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scooter_trip_routes', function (Blueprint $table) {
            $table->id();
            $table->dateTime("time");
            $table->float("coordinates_lat");
            $table->float("coordinates_long");
            $table->foreignId('scooter_trip_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scooter_trip_routes');
    }
}
