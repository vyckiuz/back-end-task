<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScooterController;
use App\Http\Controllers\ScooterEventController;
use App\Http\Controllers\ScooterTripRouteController;
use App\Http\Controllers\ScooterTripController;
use App\Http\Controllers\EventController;
use App\Http\Middleware\EnsureTokenIsValid;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::resource('events', EventController::class)->only(['index'])->middleware(EnsureTokenIsValid::class);
Route::resource('scooters', ScooterController::class)->only(['index'])->middleware(EnsureTokenIsValid::class);
Route::resource('scooters/events', ScooterEventController::class)->only(['store'])->middleware(EnsureTokenIsValid::class);

Route::resource('scooters/trips', ScooterTripController::class)->only(['index'])->middleware(EnsureTokenIsValid::class);
Route::resource('scooters/trips/routes', ScooterTripRouteController::class)->only(['store'])->middleware(EnsureTokenIsValid::class);