<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\City;
use Illuminate\Support\Facades\Http;

class ScotersEndpointTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function test_without_parameters()
    {
        $response = $this->withHeaders(['Authorization' => 'Bearer scooterrandomtoken'])->get('/api/scooters');

        $response->assertStatus(200);
    }

    public function test_with_parameters()
    {
        $city = City::all()->random();



        $parameters = [
            "filters" => [
                'status' => 1,
            ],
        ];

        $response = $this->withHeaders(['Authorization' => 'Bearer scooterrandomtoken'])->get('/api/scooters', $parameters);

        $response->assertStatus(200);
        // var_dump($response->assertStatus(200);)
    }


    public function test_with_coordinates()
    {
        // Check if scooter is in rectangular

        $city = City::all()->random();
        $start_latitude = $city->getRandomLatitude();
        $end_latitude = $city->getRandomLatitude();
        $start_longitude = $city->getRandomLongitude();
        $end_longitude = $city->getRandomLongitude();

        $min_latitude = $start_latitude;
        $max_latitude = $end_latitude;
        if ($min_latitude > $max_latitude)
        {
            $temp = $min_latitude;
            $min_latitude = $max_latitude;
            $max_latitude = $temp;
        }
        

        $min_longitude = $start_longitude;
        $max_longitude = $end_longitude;
        if ($min_longitude > $max_longitude)
        {
            $temp = $min_longitude;
            $min_longitude = $max_longitude;
            $max_longitude = $temp;
        }

        $parameters = [
            "filters" => [
                "status" => 1,
            ],
            "coordinates" => [
                'start_latitude' => $start_latitude,
                'end_latitude' => $end_latitude,
                'start_longitude' => $start_longitude,
                'end_longitude' => $end_longitude
            ]
        ];

        var_dump($parameters);


        $response = Http::withToken("scooterrandomtoken")->get("http://back-end-task.test/api/scooters", $parameters);
        // var_dump($response->json());

        var_dump($response->json());

        foreach ($response->json() as $scooter) {
            $this->assertTrue($scooter["coordinates_lat"] > $min_latitude);
            $this->assertTrue($scooter["coordinates_lat"] < $max_latitude);
            $this->assertTrue($scooter["coordinates_long"] > $min_longitude);
            $this->assertTrue($scooter["coordinates_long"] < $max_longitude);
        }

    }
}