## How to run:

- Download composer
- run compose install
- php artisan migrate - run migrations
- seed DB with cities/scooter models/users/scooters data - php artisan db:seed
- php artisan server - to server project
- php artisan executechildprocess - command to perform API requests


## DB

[DB Schema](https://gitlab.com/vyckiuz/back-end-task/-/blob/master/database/download.png)

[DB dump](urhttps://gitlab.com/vyckiuz/back-end-task/-/blob/master/database/database.sqlitel)

## ENDPOINTS

All requests need to have Authorization header

```bash
Headers:
Authentication Bearer scooterrandomtoken 
```

```bash
GET '/api/scooters'

Description:
Returns scooters data

Parameters:
coordinates   Array   ?coordinates[start_latitude]=321&coordinates[start_longitude]=123&coordinates[end_latitude]=321&coordinates[end_longitude]=123222
filters       Array.  ?filters['status']. Values: 1 Available/2 - Occupied

Example request:
curl --location -g --request GET 'http://back-end-task.test/api/scooters/?filters[status]=1&coordinates[start_latitude]=45.493562&coordinates[end_latitude]=45.667411&coordinates[start_longitude]=-73.760241&coordinates[end_longitude]=-73' \
--header 'Authorization: Bearer scooterrandomtoken'
```

```bash
POST '/api/scooters/events'

Description:
Scooter sends data when trips starts and ends

Parameters:
event_id          int       1 - trip begins/2 - trip ends
coordinates_lat   float     start/end latitude
coordinates_long  float     start/end longitude
user_uuid         string    user identifier
scotter_uuid      string    scooter identifier
time              datetime  time when trips starts/ends

Example request:
curl --location --request POST 'http://back-end-task.test/api/scooters/events/?event_id=1&coordinates_lat=45&coordinates_long=45&user_uuid=dvDPlTVS0yCSDXuAwqGlhIfwYerC7A&scooter_uuid=B1Tds6duBIOUPmsuvf2eg2lqPFmdhe&start_time=2021-01-23' \
--header 'Authorization: Bearer scooterrandomtoken'
```

```bash
GET 'api/scooters/trips'

Description:
Returns trips data

Example request:
curl --location --request GET 'http://back-end-task.test/api/scooters/trips' \
--header 'Authorization: Bearer scooterrandomtoken'
```

```bash
POST 'api/scooters/trips/routes'

Description:
Scooter send trip route update every 3 seconds

Parameters:
coordinates_lat   float     start/end latitude
coordinates_long  float     start/end longitude
user_uuid         string    user identifier
scotter_uuid      string    scooter identifier
time              datetime  time when trips starts/ends

Example request:
curl --location --request POST 'http://back-end-task.test/api/scooters/trips/routes?&coordinates_lat=45.544157&coordinates_long=-75.591988&user_uuid=5vFeub1ncoF5dJAmT9YTB5x2WEH4uc&scooter_uuid=B1Tds6duBIOUPmsuvf2eg2lqPFmdhe&time=2020-12-12%2023:53:12' \
--header 'Authorization: Bearer scooterrandomtoken'
```

```bash
GET '/api/events'

Description:
Returns all available events

Example request:
curl --location --request GET 'http://back-end-task.test/api/events' \
--header 'Authorization: Bearer scooterrandomtoken'
```

## Few tests
```bash
php artisan test
```

- Checks /api/scooters endpoint status code
- Checks if /api/scooters endpoint coordinates filters working
